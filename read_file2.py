from collections import Counter # Counter library is used to keep track of how many times equivalent values are added

def word_count(filename):
    with open(filename) as f:
        return Counter(f.read().split())
    
counter = word_count("unsorted_fruits_with_repeats.txt")
for i in counter:
    print(i, ":" , counter[i])
