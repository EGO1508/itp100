from gasp import *

begin_graphics()
finished = False



def place_player():
    global player_x, player_y, player_shape, place_player, move_player
    print("Here I am!")
    player_x = random_between(0,63)
    player_y = random_between(0,47)
    player_shape = Circle((player_x, player_y), 5)

def move_player():
    sleep(0.05)
    print("I'm moving...")
    update_when('key_pressed')
    key = update_when('key_pressed')
    if key == '6':
        player_x += 1
    elif key == '3':
        if player_x < 63:
            player_x += 1
        if player_y > 0:
            player_y -= 1
    move_to(player_shape, (10*player_x+5, 10*player_y+5))

place_player()

while not finished:
    move_player()

end_graphics()
