from gasp import *

begin_graphics()
def draw_face(x, y):
#head
    Circle((x + 300,y + 200), 100)

#eyes
    Circle((x + 250,y + 201), 20)
    Circle((x + 350,y +201), 20)

#eyebrows
    Line((x + 230,y + 225),(x + 270,y + 225))
    Line((x + 330,y + 225),(x + 370,y + 225))

#hair
    Line((x + 240,y + 250),(x + 240,y + 360))
    Line((x + 280,y + 250),(x + 320,y + 360))
    Line((x + 240,y + 360),(x + 320,y + 360))

#nose
    Line((x + 280,y + 190),(x + 300,y + 190))
    Line((x + 280,y + 190),(x + 300,y + 230))

#mouth
    Arc((x + 300, y + 200), 75, 225, 315)
#body arms and legs
    Line((x + 300, y + 100),(x + 300, y + 10))
    Line((x + 300, y + 100),(x + 250, y + 60))
    Line((x + 300, y + 100),(x + 350, y + 60))
    Line((x + 300, y + 10),(x + 250, y + 0))
    Line((x + 300, y + 10),(x + 350, y + 0))

draw_face( 0, 10)
update_when('key_pressed')
end_graphics()