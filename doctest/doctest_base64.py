def to_base(num0, base):
    """
    """
    """
    Convert an decimal integer into a string representation of the digits
    representing the number in the base (between 2 and 10) provided.

      >>> to_base(10, 3)
      '101'
      >>> to_base(11, 2)
      '1011'
      >>> to_base(10, 6)
      '14'
    """ 
    convertString = "0123456789ABCDEF" # letters and numbers used up to hex
    if num0 < base:
        return convertString[num0] # convert last number if num 0 is less than base by looking its string equivalent in the sequence
    else:
        return to_base(num0//base,base) + convertString[num0%base] # Hard to explain
    return to_base(num0,base) # return the function using num0 base

to_base(10,3)
to_base(11,2)
to_base(10,6)

def to_binary(num):
    """
    """
    """
    Convert an decimal integer into a string representation of its binary
    (base2) digits.

      >>> to_binary(10) 
      '1010'
      >>> to_binary(12) 
      '1100'
      >>> to_binary(0) 
      '0'
      >>> to_binary(1) 
      '1'
    """
    def to_base(num, base):

        convertString = "0123456789ABCDEF"
        if num < base:
            return convertString[num]
        else:
            return to_base(num//base,base) + convertString[num%base]
        return to_base(num,2)

to_binary(10)
to_binary(12)
to_binary(0)
to_binary(1)

def to_base3(num3):
    """
    """
    """
    Convert an decimal integer into a string representation of its base3
    digits.

      >>> to_base3(10)
      '101'
      >>> to_base3(12)
      '110'
      >>> to_base3(6)
      '20'
    """

    def to_base(num3, base):

        convertString = "0123456789ABCDEF"
        if num3 < base:
            return convertString[num3]
        else:
            return to_base(num3//base,base) + convertString[num3%base]
    return to_base(num3,3)  

to_base3(10)
to_base3(12)
to_base3(6)

def to_base4(num4):
    """
    """
    """
    Convert an decimal integer into a string representation of its base4
    digits.

      >>> to_base4(20)
      '110'
      >>> to_base4(28)
      '130'
      >>> to_base4(3)
      '3'
    """
    def to_base(num4, base):
        convertString = "0123456789ABCDEF"
        if num4 < base:
            return convertString[num4]
        else:
            return to_base(num4//base,base) + convertString[num4%base]
    return to_base(num4,4)  

to_base4(20)
to_base4(28)
to_base4(3)




if __name__ == "__main__":
    import doctest
    doctest.testmod()
