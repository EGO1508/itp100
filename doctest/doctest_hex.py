def to_base(num, base):
    """
    """
    """
    Convert an decimal integer into a string representation of the digits
    representing the number in the base (between 2 and 16) provided.

      >>> to_base(10, 3)
      '101'
      >>> to_base(11, 2)
      '1011'
      >>> to_base(10, 6)
      '14'
      >>> to_base(21, 3)
      '210'
      >>> to_base(21, 11)
      '1A'
      >>> to_base(47, 16)
      '2F'
      >>> to_base(65535, 16)
      'FFFF'
      >>> to_base(14, 64)
      'O'
      >>> to_base(63, 64)
      '/'
    """
    if num == 0:
        return "0"
    numstr = ''
    digits = '0123456789ABCDEF'
    base64 = 'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789+/'
    while num:
        numstr = digits[num % base] + numstr
        num //= base
    if base == 64:
        numstr = base64[num % base] + numstr
        num //= base
    
def base64encode(three_bytes):
    """
    """
    """
      >>> base64encode(b'\\x5A\\x2B\\xE6')
      'Wivm'
      >>> base64encode(b'\\x49\\x33\\x8F')
      'STOP'
      >>> base64encode(b'\\xFF\\xFF\\xFF')
      '////'
      >>> base64encode(b'\\x00\\x00\\x00')
      'AAAA'
    """
    digits = 'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789+/'

    try:
        # turn the three bytes into ints
        b1, b2, b3 = three_bytes[0], three_bytes[1], three_bytes[2]
        # get first 6 bits of b1 
        index1 = b1 >> 2
        # join last 2 bits of b1 shifted left 4 with first 4 bits of b2
        index2 = (b1 & 3) << 4 | b2 >> 4
        # join last 4 bits of b2 shifted left 2 with first 2 bits of b3
        index3 = (b2 & 15) << 2 | (b3 & 192) >> 6
        # get last 6 bits of b3
        index4 = b3 & 63
    except (AttributeError, TypeError):
        raise AssertionError('Input should be 3 bytes')

    return f'{digits[index1]}{digits[index2]}{digits[index3]}{digits[index4]}'

def base64decode(four_chars):
    """
    >>> base64decode('STOP')
    b'I3\\x8f'
	"""
    digits = 'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789+/'
    ints = [digits.index(ch) for ch in four_chars]
    b1 = (ints[0] << 2) | ((ints[1] & 48) >> 4)
    b2 = (ints[1] & 15) << 4 | ints[2] >> 2
    b3 = (ints[2] & 2) << 6 | (ints[3] & 15) << 0
    # 143
    return bytes([b1, b2, b3])






# This below was an attemp to do the thing above

    # Code From https://www.geeksforgeeks.org/python-split-string-into-list-of-characters/
def split(word):
    return [char for char in word]
# Code From https://stackoverflow.com/questions/29513202/splitting-the-digits-of-an-integer-into-groups-of-1-2-3-etc-digits
def split2( seq, n ):
    """A generator to divide a sequence into chunks of n units."""
    seq = str(seq)
    while seq:
        yield int(seq[:n])
        seq = seq[n:]

def base64decode2(three_bytes):
    """
    """
    """
      >>> base64encode('Wivm')
      b'\\x5A\\x2B\\xE6'
      >>> base64encode('STOP')
      b'\\x49\\x33\\x8F'
    """
    digits = 'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789+/' 
    n = three_bytes
    c = split(n)
    nums = ''
    for word in c:
        m = digits.find(word)
        nums += str(m)
    splited_nums = split2(nums,2)
    splited_nums = list(splited_nums)
    b1, b2, b3 = splited_nums[0], splited_nums[1], splited_nums[2]
    b1 = bin(b1)
    b2 = bin(b2)
    b3 = bin(b3)
    #print(splited_nums)
    #print(b2)
    #print(b3)
#base64decode2('STOP')





if __name__ == '__main__':
    import doctest
    doctest.testmod()
