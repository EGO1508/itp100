from collections import Counter # Counter library is used to keep track of how many times equivalent values are added

def word_count(filename):
    with open(filename) as f:
        return Counter(f.read().split())
    
counter = word_count("alice_in_wonderland.txt")
for i in counter:
    print(i, ":" , counter[i])