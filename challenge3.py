from gasp import *
begin_graphics()
#challenge 1
#goto robots.py

#challenge 2
#goto robots.py

#challenge 3
#ball_x = 5
#ball_y = 5
#c = Circle((ball_x, ball_y),5)
#while ball_x <= 635:
#    sleep(0.02)
#    ball_x += 2.0 
#    ball_y += 1.5 
#    move_to(c, (ball_x, ball_y))

#challenge 4

key_text = Text("a", (320, 240), size=48)

while True:
    key = update_when('key_pressed')
    remove_from_screen(key_text)
    key_text = Text(key, (320, 240), size=48)
    if key == 'q':     # See Sheet C if you don't understand this
        break
end_graphics()
