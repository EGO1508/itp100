

def encode3bytes(bytes):
    """
    Convert 3 bytes into 4 base64 encoded characters. Pads with = or == when
    it recieves only 1 or 2 bytes as an argument respectively.

      >>> encode3bytes(b'\\x5A\\x2B\\xE6')
      'Wivm'
      >>> encode3bytes(b'\\x49\\x33\\x8F')
      'STOP'
      >>> encode3bytes(b'\\xFF\\xFF\\xFF')
      '////'
      >>> encode3bytes(b'\\x00\\x00\\x00')
      'AAAA'
      >>> encode3bytes(b'T')
      'VA=='
      >>> encode3bytes(b'Te')
      'VGU='
      >>> encode3bytes(b'Tst')
      'VHN0'
      >>> encode3bytes(b'\\x00')
      'AA=='
      >>> encode3bytes(b'\\x00\\x00')
      'AAA='
      >>> encode3bytes(b'\\xFF')
      '/w=='
      >>> encode3bytes(b'\\xFF\\xFF')
      '//8='
    """
    digits = 'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789+/'

    if len(bytes) < 1 or len(bytes) > 3:
        raise ValueError('Input should be 1 to 3 bytes')

    # get first byte and first base64 character (6 bits) from it
    b1 = bytes[0]
    index1 = b1 >> 2

    # Handle one byte case
    if len(bytes) == 1:
        # 2nd base64 character is 2 lower order bits left shifted by 4
        index2 = (b1 & 3) << 4
        return f'{digits[index1]}{digits[index2]}=='

    b2 = bytes[1]
    # join last 2 bits of b1 shifted left 4 with first 4 bits of b2
    index2 = (b1 & 3) << 4 | b2 >> 4

    # Handle two byte case
    if len(bytes) == 2:
        # 3rd base64 character is lower order 4 bits of 2nd byte left shited 2
        index3 = (b2 & 15) << 2
        return f'{digits[index1]}{digits[index2]}{digits[index3]}='

    b3 = bytes[2]
    # join last 4 bits of b2 shifted left 2 with first 2 bits of b3
    index3 = (b2 & 15) << 2 | (b3 & 192) >> 6
    # get last 6 bits of b3
    index4 = b3 & 63
    return f'{digits[index1]}{digits[index2]}{digits[index3]}{digits[index4]}'


def decode4chars(s):
    """
    Return 4 base64 encoded characters to 3 bytes from which they orginated.
    Handle special endings == and = where only 1 or 2 bytes are returned
    repectively.

      >>> decode4chars('STOP')
      b'I3\\x8f'
      >>> decode4chars('Wivm')
      b'Z+\xe6'
      >>> decode4chars('////')
      b'\xff\xff\xff'
      >>> decode4chars('VA==')
      b'T'
      >>> decode4chars('VGU=')
      b'Te'
      >>> decode4chars('AA==')
      b'\\x00'
      >>> decode4chars('AAA=')
      b'\\x00\\x00'
      >>> decode4chars('/w==')
      b'\\xff'
      >>> decode4chars('//8=')
      b'\\xff\\xff'
    """
    digits = 'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789+/'

    if len(s) != 4 or not all([ch in digits + '=' for ch in s]):
        raise ValueError(f'{s} is not a value base64 encoded string')

    # Compute first byte
    int1 = digits.index(s[0])
    int2 = digits.index(s[1])
    b1 = (int1 << 2) | ((int2 & 48) >> 4)

    # Handle single byte return case
    if s[2:] == '==':
        return bytes([b1])

    int3 = digits.index(s[2])
    b2 = (int2 & 15) << 4 | int3 >> 2

    # Handle 2 byte return case
    if s[3:] == '=':
        return bytes([b1, b2])

    int4 = digits.index(s[3])
    b3 = (int3 & 3) << 6 | int4

    return bytes([b1, b2, b3])

file_object = open('cat.jpeg','rb+')

def base64encode(f):
    """
    Read the contents of a binary file and return a base64 encoded string.
    """
    s = ''
    data = f.read(3)

    while data:
        s += encode3bytes(data)
        data = f.read(3)

    return s

base64encode(file_object)

f = open("D:\encoded_image.txt","w")

def base64decode(f):
    """
    """
    s = ''
    data = f.read(3)

    while data:
        s += decode4chars(data)
        data = f.read(3)
        print(f.read)

    return s

base64decode(f)


if __name__ == '__main__':
    import doctest
    doctest.testmod()
