from gasp import *

begin_graphics()
#head
Circle((300,200), 100)

#eyes
Circle((250,201), 20)
Circle((350,201), 20)

#eyebrows
Line((230,225),(270,225))
Line((330,225),(370,225))

#hair
Line((240,250),(240,360))
Line((280,250),(320,360))
Line((240,360),(320,360))

#nose
Line((280,190),(300,190))
Line((280,190),(300,230))

#mouth
Arc((300,200), 75, 225, 315)


update_when('key_pressed')
end_graphics()


